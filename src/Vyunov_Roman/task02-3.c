#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

int main()
{
	unsigned int begin, end;
	printf("Enter interval as [number-number]: ");
	scanf("%d-%d", &begin, &end);
	for (begin; begin <= end; begin++)
	{
		printf("%d ",begin);
	}
	printf("\n");
	return 0;
}